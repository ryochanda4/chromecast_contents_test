<?php

header("Pragma: no-cache");
header("Cache-Control: no-cache");

function oreno_log($message)
{
    date_default_timezone_set('Asia/Tokyo');
    $timestamp=time();
    $timestamp_m=date("Y/m/d H:i:s", $timestamp);
    error_log("[${timestamp_m}] ${message}\n", 3, "./logs/php.log");
}

// delete cookie
/*
if (isset($_COOKIE["beacon"])){
    setcookie("uliza_iphone_auth", 'pYVasTy3P0VGdlxttgFksqsE4/5zDSz5Wu09W7Xbssc=?guid=d6e3a83b-dfff-45c3-b497-d85896f2d26d&reciverurl=https://wvpkg01.uliza.jp&v=117989:1407468937,117986:1407468937,117988:1407468937,117987:1407468937&_expire=1407472537', time() - 1800);
	oreno_log('[INFO] delte cookie');
}
*/
if(!isset($_COOKIE["beacon"])){
    setcookie("beacon", 'aaaaaaaa', time() + 60);
    oreno_log('[ERROR] beacon cookie not found , set cookie');
    $pos = $_GET['pos'];
    if($pos > 60){
        //header("HTTP/1.0 401 Unauthorized");
        exit();
    }
}

//oreno_log('[SUCCESS] beacon cookie found: ' + $_COOKIE["beacon"]);
header("HTTP/1.1 200 OK");
exit();

?>

