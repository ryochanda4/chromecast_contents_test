<?php
function oreno_log($message)
{
    date_default_timezone_set('Asia/Tokyo');
    $timestamp=time();
    $timestamp_m=date("Y/m/d H:i:s", $timestamp);
    error_log("[${timestamp_m}] ${message}\n", 3, "./logs/php.log");
}

$stream=$_GET["stream"];
header("Content-Type: binary/octet-stream");
header("Pragma: no-cache");
header("Cache-Control: no-cache");
header("Access-Control-Allow-Credentials: true");

if(!isset($_COOKIE["uliza_iphone_auth"])){
    oreno_log('[ERROR] cookie is not set');
    header('HTTP', true, 403);
    exit;
}
/*}else if ($_COOKIE["uliza_iphone_auth"] != '1' ){
    oreno_log('[ERROR] invalid cookie found: '.$_COOKIE["uliza"]);
    header('HTTP', true, 403);
    exit;
}*/

oreno_log('[SUCCESS] cookie found: uliza_iphone_auth='.$_COOKIE["uliza_iphone_auth"]);

//echo hex2bin(file_get_contents($Stream.".txt"));
echo file_get_contents("keys/".$stream.".bin");
exit();
?>
